using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billiard : MonoBehaviour
{
    [SerializeField] private float speed = 10;
    [SerializeField] private Rigidbody Sphere;
    // Start is called before the first frame update
    void Start()
    {
        Sphere.AddForce(0, 0, speed, ForceMode.Impulse);
    }

}
