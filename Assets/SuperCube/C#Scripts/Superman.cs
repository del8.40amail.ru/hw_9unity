using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Superman : MonoBehaviour
{
    [SerializeField] GameObject superCube;
    public MovementAlongThePath MovementAlongThePath;
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.GetComponent<Rigidbody>())
            rb.AddForce(superCube.transform.position * MovementAlongThePath.power, ForceMode.Impulse);
    }
}