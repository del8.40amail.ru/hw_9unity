using System;
using UnityEngine;
public class MovementAlongThePath : MonoBehaviour
{
    [SerializeField] public float power = 10;
    [SerializeField] private float speed = 0;
    private float maxDistance = 0.9f;
    [SerializeField] private GameObject[] people;
    private Rigidbody superCube;
    private int peopleIndex = 0;

    private void Start()
    {
        superCube = GetComponent<Rigidbody>();
    }
    void Update()
    {
        superCube.transform.position = Vector3.MoveTowards(superCube.transform.position, people[peopleIndex].transform.position, Time.deltaTime * speed);
        superCube.transform.LookAt(people[peopleIndex].transform.position);
        var distanceSqure = (superCube.transform.position - people[peopleIndex].transform.position).sqrMagnitude;
        if (distanceSqure < maxDistance)
        {
            peopleIndex++;
            if (peopleIndex > people.Length-1)
            {
                peopleIndex = 0;
            }
        }
    }
}