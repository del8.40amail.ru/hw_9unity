using UnityEngine;

public class CreatingAPath : MonoBehaviour
{
    public Transform[] PathElements;

    public void OnDrawGizmos()
    {
        if(PathElements== null || PathElements.Length<2)
        {
            return;
        }
        for (int i = 1; i < PathElements.Length; i++)
        {
            Gizmos.DrawLine(PathElements[i - 1].position, PathElements[i].position);
        }
    }
}
