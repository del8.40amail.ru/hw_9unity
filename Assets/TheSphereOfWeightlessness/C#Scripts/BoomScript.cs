using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomScript : MonoBehaviour
{
    [SerializeField] private float timeToExplosion = 3;
    [SerializeField] private float power = 10;
    [SerializeField] private float Radius = 100;

    void Update()
    {
        timeToExplosion -= Time.deltaTime;

        if (timeToExplosion <= 0)
        {
            Boom();
        }
    }
    void Boom()
    {
        Rigidbody[] shapes = FindObjectsOfType<Rigidbody>();

        foreach (Rigidbody shape in shapes)
        {
            var dataDistance = Vector3.Distance(transform.position, shape.transform.position);
            if (dataDistance < Radius) 
            {
                Vector3 direction = shape.transform.position - transform.position;
                shape.AddForce(direction.normalized * power * (Radius - dataDistance),ForceMode.Impulse);
            }
        }
        timeToExplosion = 3;

    }
}
